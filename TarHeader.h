
#ifndef _TARHEADER_H_
#define _TARHEADER_H_

#define HBLK_SIZE 512

#include <cstring>

class TarHeader {
private:
    char buffer[HBLK_SIZE];
    int start;

    // old format fields
    char fname[100]; // file name
    char fmode[8]; // file mode
    char owner_id[8]; // owner's numeric user id
    char group_id[8]; // group's numeric user id
    int  fsize; // file size in octal base
    char last_mod[12]; // last modification time in (UNIX numeric time format (octal)
    char checksum[8]; // checksum for header record
    char link[1]; // link indicator (file type)
    char link_name[100]; // name of linked file

    // USTar fields
    // bytes 0 - 156 are old fmt

    char ustar[6]; // UStar indicator "ustar" then NUL
    char ustar_ver[2]; // UStar version "00"
    char ustar_owner[32]; // owner user name
    char ustar_group[32]; // owner group name
    char ustar_dev_maj[8]; // device major number
    char ustar_dev_min[8]; // device minor number
    char ustar_prefix[155]; // filename prefix


    bool is_dir;
    friend class TarFile;
//    friend class TarHeader;
public:
    TarHeader clone();
};

#endif // _TARHEADER_H_
